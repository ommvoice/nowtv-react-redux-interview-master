import configStore from 'redux-mock-store';

const defaultMiddleware = [];
const configureStore = (middleware = defaultMiddleware) =>
  configStore(middleware);

export { configureStore };
