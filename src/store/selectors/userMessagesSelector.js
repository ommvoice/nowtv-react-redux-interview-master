import _ from 'underscore';
import { createSelector } from 'reselect';

export const getUserMessages = (state) => {
    const messages = state.messages.payload;
    const members = state.members.payload;

    if( messages && members ) {
        messages.map(message => {
        const userFound = members.find(member => member.id === message.userId);

        message.email = userFound.email;
        message.avatar = userFound.avatar;
        });
    }

    return messages;
}

export const getSortedUserMessages = createSelector(
    getUserMessages,
    messages => _.sortBy(messages, 'timestamp'),
)
