import React from 'react';
import MessagesCard from '../../organisms/MessagesCard'

class MessageList extends React.Component {
  componentDidMount() {
    this.props.loadMessages();
    this.props.loadMembers();
  }

  render() {
    return this.props.messages.length > 0 && <MessagesCard messages={this.props.messages} />
  }
}

export default MessageList
