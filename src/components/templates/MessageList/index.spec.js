import React from 'react';
import { shallow, } from 'enzyme';
import MessageList from '.';

describe('Templates/MessageList', () => {
  it('renders correctly', () => {
    const props = {
      messages: [],
      loadMessages: () => {},
      loadMembers: () => {},
    };
    const tree = shallow(<MessageList {...props} />);
    expect(tree).toMatchSnapshot();
  });
});
