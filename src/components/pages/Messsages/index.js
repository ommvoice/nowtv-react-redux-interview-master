import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { loadMessages } from '../../../action-creators/messages';
import { loadMembers } from '../../../action-creators/members';
import MessageList from '../../templates/MessageList';
import { getSortedUserMessages} from '../../../store/selectors/userMessagesSelector';

const mapStateToProps = state => ({
  messages: getSortedUserMessages(state), 
});

const mapDispatchToProps = dispatch => bindActionCreators({ loadMessages, loadMembers }, dispatch);

MessageList.propTypes = {
  loadMessages: PropTypes.func,
  loadMembers: PropTypes.func,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MessageList);
