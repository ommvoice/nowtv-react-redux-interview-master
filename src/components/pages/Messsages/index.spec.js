import React from 'react';
import { shallow, } from 'enzyme';
import { configureStore } from '../../../utils/mocks';
import Index from '.';

describe('Pages/Messages', () => {
  it('renders correctly', () => {
    const mockStore = configureStore();
    const store = mockStore({
      messages: {
        payload: []
      },
      members: {
        payload: []
      }
    })

    const props = {
      messages: [],
      loadMessages: () => {},
      loadMembers: () => {},
    };
    const tree = shallow(<Index store={store}  {...props} />);
    expect(tree).toMatchSnapshot();
  });
});
