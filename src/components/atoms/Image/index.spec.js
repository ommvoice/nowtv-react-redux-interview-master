import React from 'react';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Image from '.';

configure({ adapter: new Adapter() });

describe('Atoms/Image', () => {
  it('renders correctly', () => {
    const render = shallow(<Image src='src' />);

    expect(render).toMatchSnapshot();
  });
});
