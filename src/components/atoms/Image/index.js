import React from 'react';

const Image = ({
    src,
    ...otherProps
  }) => (
    <img
        alt="avatar"
        className="message-image"
        src={`${src}`}
        {...otherProps}
    />
  );

  export default Image;
  