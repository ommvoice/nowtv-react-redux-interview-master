import React from 'react';
import Image from '../../atoms/Image';
import './MessageCard.css';
import Moment from 'moment';

const FormattedDate = ({ message }) => {
    const datetimeFormat = 'MMMM Do YYYY, h:mm:ss A';
    return (Moment(new Date(message.timestamp)).format(datetimeFormat))
}

class MessageCard extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
          hover: false,
        };
    }

    handleMouseHover() {
        this.setState(this.toggleHoverState);
    }

    toggleHoverState(state) {
        return {
          hover: !state.hover,
        };
    }

    render() {
        const { message } = this.props;

        return (
            <div className="message-card">
                <div><Image src={message.avatar} /></div>
                <div className="message-card-hover">
                    <div 
                      onMouseEnter={() => this.handleMouseHover()} 
                      onMouseLeave={() => this.handleMouseHover()}>{message.message}
                    </div>
                        {
                            this.state.hover &&
                            <div style={{color: 'red'}}>{message.email}</div>
                        }            
                    </div>
                <div> 
                <FormattedDate message={message} />
                </div>
           </div>
        )}
}

export default MessageCard;
