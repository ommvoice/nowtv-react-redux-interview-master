
import React from 'react';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import MessageCard from '.';

describe('Molecuses/MessageCard', () => {
    const mockMessage =     {
        "id": "945da807-f6b9-4aa8-8baf-156c9061e00a",
        "userId": "898fc6d4-5706-44d2-b1f3-eb8908e8a3c3",
        "message": "Nunc rhoncus dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci.",
        "timestamp": "2016-09-16T07:50:43Z",
        "avatar": "test:jpg",
        "userId": "1"
    };

  it('renders correctly', () => {
    const render = shallow(<MessageCard message={mockMessage} />);

    expect(render).toMatchSnapshot();
  });
});
