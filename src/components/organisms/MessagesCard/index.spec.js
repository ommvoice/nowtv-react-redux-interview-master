import React from 'react';
import { shallow, configure } from 'enzyme';
import MessagesCard from '.';


describe('Organisms/MessagesCard', () => {
    const mockMessages = [
    {
        "id": "945da807-f6b9-4aa8-8baf-156c9061e00a",
        "userId": "898fc6d4-5706-44d2-b1f3-eb8908e8a3c3",
        "message": "Nunc rhoncus dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci.",
        "timestamp": "2016-09-16T07:50:43Z",
        "avatar": "test:jpg",
        "userId": "1"
      },
      {
        "id": "c8212810-eb89-4f19-ae77-6f844a7cb98a",
        "userId": "0d9496c4-2490-40e6-a0c3-0964e371f1ab",
        "message": "Vivamus in felis eu sapien cursus vestibulum. Proin eu mi. Nulla ac enim.",
        "timestamp": "2016-11-02T00:09:49Z",
        "avatar": "test2:jpg",
        "userId": "2"
      },
  ]

  it('renders correctly', () => {
    const render = shallow(<MessagesCard messages={mockMessages} />);

    expect(render).toMatchSnapshot();
  });
});
