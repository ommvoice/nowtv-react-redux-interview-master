import React from 'react';
import MessageCard from '../../molecules/MessageCard';

const MessagesCard = ({
    messages
  }) => {
    return messages.map(message => (
        <MessageCard key={message.id} message={message} />
       ));
    };

  export default MessagesCard;
  