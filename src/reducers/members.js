import { MEMBERS_LOADED } from '../action-creators/members/action-types';

export default (state = [], action) => {
    switch (action.type) {
      case MEMBERS_LOADED:
        return { ...state, payload: action.payload }
      default:
        return state
    }
  }