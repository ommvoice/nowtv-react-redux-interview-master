import { MESSAGES_LOADED } from '../action-creators/messages/action-types';

export default (state = [], action) => {
    switch (action.type) {
      case MESSAGES_LOADED:
        return { ...state, payload: action.payload }
      default:
        return state
    }
  }